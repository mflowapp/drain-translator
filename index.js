#!/usr/bin/env node

"use strict";

var fs = require("fs");
var program = require("commander");
var chalk = require("chalk");
var drnTranslator = require("./draintranslator");

console.log("Drain package translator");

program
    .arguments("<intputFile> <outputFile>")
    .action(function (inputFile, outputFile) {
        console.log("Input file: %s\nOutput file: %s\n", inputFile, outputFile);
        try {
            var payloadString = fs.readFileSync(inputFile, "utf8");
            var inputData = JSON.parse(payloadString);
            drnTranslator.translate(inputData, outputFile);
        } catch (err) {
            console.error(chalk.red(err.stack));
            process.exit(1);
        }
    })
    .parse(process.argv);

