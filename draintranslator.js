"use strict";

const fs = require("fs");

// get the index and time span of the first span arguement in the second schedule argument
var getIndexAndTimeSpan = function (span, schedule) {
    for (let i = 0; i < schedule.length; i++) {
        let t = schedule[i];
        if (span[0] >= t[0] && span[1] <= t[1]) {
            return {
                "index": i,
                "timeSpan": t
            };
        }
    }
    return null;
};

var createDrnPackageString = function (drnCells, drnPackageSettings) {
    let drnPackageString = "# DRN: Drain package file created on " + new Date() + "\n";

    let MAXACTD = drnCells.drainCellsCount;
    let IDRNCB = drnPackageSettings.IDRNCB;
    let dataSet2 = "\t" + MAXACTD + "\t" + IDRNCB + "\n";
    drnPackageString += dataSet2;

    let drainCellsForAllStressPeriods = drnCells.drainCells;
    for (let i = 0; i < drainCellsForAllStressPeriods.length; i++) {
        let drainCellsForOneStressPeriod = drainCellsForAllStressPeriods[i];
        let ITMP = Object.keys(drainCellsForOneStressPeriod).length;
        let NP = 0;

        let dataSet5 = "\t" + ITMP + "\t" + NP + "\n";
        drnPackageString += dataSet5;

        let dataSet6b = "";
        for (let cell in drainCellsForOneStressPeriod) {
            let elevation = drainCellsForOneStressPeriod[cell].elev * drnPackageSettings.lengthUnitFactor;
            let cond = drainCellsForOneStressPeriod[cell].cond * drnPackageSettings.conductanceUnitFactor;

            dataSet6b += cell + "\t" + elevation + "\t" + cond + "\n";
        }
        drnPackageString += dataSet6b;
    }

    return drnPackageString;
};

var translate = function (inputData, drnPackageFullPath) {
    try {
        console.log("Translating DRN package...");

        let modelRunTimeSchedule = inputData["modelRunTimeSchedule"];
        let drainGroups = inputData["drainGroups"];
        let drnPackageSettings = inputData["drnPackageSettings"];

        let drainCellsCount = 0;
        let drainCells = []; // each item in the array is drain cells for one stress period

        let modelSchedule = modelRunTimeSchedule.schedule;
        for (let i = 0; i < modelSchedule.length; i++) {
            let modelTimeSpan = modelSchedule[i];

            let drainCellsForOneStressPeriod = {};
            for (let j = 0; j < drainGroups.length; j++) {
                let drainGroup = drainGroups[j];

                let drainGroupCells = drainGroup.groupCells;
                let drainGroupSchedule = drainGroup.timeSchedule.schedule;
                let drainGroupAttributes = drainGroup.attributes;

                drainCellsCount += drainGroupCells.length;

                let indexSpan = getIndexAndTimeSpan(modelTimeSpan, drainGroupSchedule);
                if (indexSpan === null) {
                    let attributes = drainGroupAttributes[drainGroupAttributes.length - 1];
                    for (let l = 0; l < drainGroupCells.length; l++) {
                        let cell = drainGroupCells[l];
                        let elevAtCell = attributes.elev[l];
                        let condAtCell = attributes.cond[l];

                        drainCellsForOneStressPeriod[cell] = {
                                    "elev": elevAtCell,
                                    "cond": condAtCell
                                };
                    }
                } else {
                    for (let k = 0; k < drainGroupAttributes.length; k++) {
                        if (drainGroupAttributes[k].timeSpan === indexSpan.index) {
                            let elev = drainGroupAttributes[k].elev;
                            let cond = drainGroupAttributes[k].cond;

                            for (let l = 0; l < drainGroupCells.length; l++) {
                                let cell = drainGroupCells[l];
                                let elevAtCell = elev[l];
                                let condAtCell = cond[l];

                                drainCellsForOneStressPeriod[cell] = {
                                    "elev": elevAtCell,
                                    "cond": condAtCell
                                };
                            }
                        }
                    }
                }
            }

            drainCells.push(drainCellsForOneStressPeriod);
        }

        drainCellsCount = drainCellsCount / modelSchedule.length;

        let content = createDrnPackageString({"drainCellsCount": drainCellsCount, "drainCells": drainCells}, drnPackageSettings);
        fs.writeFileSync(drnPackageFullPath, content, "utf8");
        console.log("Drain package translation completed!");
    } catch (err) {
        throw (err);
    }
};

module.exports = {
    translate: translate
};
