# Drain Translator#

Drain translator is a command line Node.js application that translates cell level drain boundary condition to DRN package file. It is built on Node.js v6.9.3.

### Command Line Format ###

1. node index <file path of payload.json> <file path of *.drn>
1. translatedrain <file path of payload.json> <file path of *.drn>

### Content of payload.json file ###


```
#!javascript

{
	"modelRunTimeSchedule": {
		"startDateTime": "2000-01-01",
        "unit": "day",
        "relative": true,
        "schedule": [[ 0, 5], [5, 10], [10, 15], [15, 20], [20, 25] ]
	},
	"drainGroups": [
		{
            "group": "drain1",
            "groupCells": [1, 4, 7],
            "timeSchedule": {
                "startDateTime": "2000-01-01",
                "unit": "day",
                "relative": true,
                "schedule": [ [0, 10], [10, 20] ]
            },
            "attributes": [
                {"timeSpan": 0, "elev": [200, 200, 200], "cond": [1, 1, 1]},
                {"timeSpan": 1, "elev": [250, 250, 250], "cond": [2, 2, 2]}
            ]
        },
        {
            "group": "drain2",
            "groupCells": [3, 6, 9],
            "timeSchedule": {
                "startDateTime": "2000-01-01",
                "unit": "day",
                "relative": true,
                "schedule": [ [0, 15], [15, 25] ]
            },
            "attributes": [
                {"timeSpan": 0, "elev": [300, 300, 300], "cond": [3, 3, 3]},
                {"timeSpan": 1, "elev": [350, 350, 350], "cond": [4, 4, 4]}
            ]
        }
	],
    "drnPackageSettings": {
        "IDRNCB": 1,
        "lengthUnitFactor": 2,
        "conductanceUnitFactor": 3
    }
}

```

### task manager call
drain input as the dataPath of the payload json file:
```
#!javascript

{
    "taskid":"taskid",
        "input": [
        {
            "name": "draininput",
            "format": "json",
            "dataPath": "/storage/draininput.json"
        }
    ],
    "outputConfig": [
        {
            "name": "drainoutput",
            "format": "drn",
            "dataPath": "/storage/drainresult.drn"
        }
    ]
}

```